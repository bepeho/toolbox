#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Convert a JSON to a graph."""

from __future__ import print_function
import json
import os
import sys
from pprint import pprint
from shutil import copyfile
import fileinput
import re
import urllib
from wordpress_xmlrpc import Client, WordPressPost
from wordpress_xmlrpc.methods import posts
import xmlrpclib
from wordpress_xmlrpc.compat import xmlrpc_client
from wordpress_xmlrpc.methods import media, posts
import os
import logging
import sys
import traceback
import subprocess 
from httplib import responses
import youtube_upload
import logging
import logging.handlers
from numpy.distutils.fcompiler import none
from youtube_upload.lib import catch_exceptions


def getMeta(meta):
    with open(meta, 'r') as f:
        data = json.load(f)
    return data
    
def postToBepeho(client, meta, attachments):
    
    data=getMeta(meta)
    
    post = WordPressPost()
    post.title = data['articleTitle']
    post.content = data['articleContent']
    for snapshot in attachments:
        post.content = post.content +'</br><img class="alignnone size-medium wp-image-240 img-responsive" src="'+snapshot['url']+'" alt="" width="300" height="169" />'
 #   post.terms_names = data['terms_names']
    post.post_status = data['post_status']
    post.thumbnail = attachments[0]['attachment_id']
    post.id = client.call(posts.NewPost(post))
    
def upload(client, root, type):
    responses = list()
    if not os.path.exists(root):
        logging.warning("{} folder does not exists".format(root))
        return 0
    for filename in os.listdir(root):
        # prepare metadata
        data = {
            'name': os.path.basename(filename),
            'type': type,  # mimetype
            'overwrite': True

        }

        # read the binary file and let the XMLRPC library encode it into base64
        with open(root+filename, 'rb') as img:
            data['bits'] = xmlrpc_client.Binary(img.read())

        response = client.call(media.UploadFile(data))
        responses.append(response)
    return responses

def uploadToYouTube(root):
    """
    Upload a folder to youtube 

    Parameters
    ----------
    root : str
        the folder to import to WP

    Examples
    --------
    >>> r = '/home/patrice/toolbox/python/pwp/test/oceanrecorder20180731_174243'

    >>> uploadToYouTube(r)
    0
    """
    meta=getMeta(root+'/meta/meta.json')
    for filename in os.listdir(root+'/videos'):
        cmd=['youtube-upload',
            '--title="testing"',
            '--description="Bepeho upload test"',
            '--category=Sports',
           # '--tags="mutter, beethoven"',
            '--recording-date=2011-03-10T15:32:17.0Z',
             '--playlist="My favorite music"',
            # '--location=(latitude=5.2404128,longitude=43.2803051)',
             '--privacy=private',
             '/home/patrice/toolbox/python/pwp/test/oceanrecorder20180731_174243/videos/Cam121478880527.mp4']
        d=['youtube-upload',
            '--title="'+str(meta['articleTitle'])+'"',
            '--description="'+str(meta['articleContent'])+'"',
            '--category='+str(meta['category']),
            '--recording-date='+str(meta['date']),
            #'--default-language="'+str(meta['langage'])+'"',
            #'--auth-browser',
            '--playlist="'+str(meta['playlist'])+'"',
            #'--location="'+str(meta['location'])+'"',
            '--privacy='+str(meta['privacy']),
            root+'/videos/'+filename
            ]
        
        #+" "+  os.path.abspath(filename)
 #       +" --tags="+meta['terms_names']\
        print ("d  :", d)
        print ("cmd:", cmd)
        ret= subprocess.call(d)

        #print("printing")
        #print (["youtube-upload"] + [k + "=" + v for k, v in d.items()])
        #ret= subprocess.call(["youtube-upload"] + [k + "=" + v for k, v in d.items()],shell=True)

def processUploadToYouTube(root):
    try:
    
        if not os.path.exists(root+'/youtube.to'):
            logging.debug("{} no export to youtube".format(root))
            return 0
        
        uploadToYouTube(root)
        os.rename(root+'/youtube.to', root+'/youtube.done')
    except:
        logging.exception('Got exception on main handler')
        os.rename(root+'/youtube.to', root+'/youtube.error')
    
    
def upoadToBepeho(root, u, p, s):
    """
    Upload a folder to bepeho WP 

    Parameters
    ----------
    root : str
        the folder to import to WP

    Examples
    --------
    >>> r = './test/oceanrecorder20180731_174243'
    >>> u = 'demo'
    >>> p = 'demo'
    >>> s = 'http://localhost:8080/xmlrpc.php'
    >>> upoadToBepeho(r,u,p,s)
    0
    """
    client = Client(url=s, username=u, password=p)
    
    snapshots=upload(client, root+'/snapshots/','image/jpeg')
    postToBepeho(client, root+'/meta/meta.json', snapshots)
    return 0
    
def processUploadToBepeho(root):
    try:
        inFile=root+'/bepeho.to'
        if not os.path.exists(inFile):
            logging.debug("{} no export to bepeho".format(inFile))
            return 0
        
        with open(inFile, 'r') as f:
            bepehoPrm = json.load(f)
        upoadToBepeho(root,u=bepehoPrm['u'], p=bepehoPrm['p'],s=bepehoPrm['s'])
        os.rename(inFile, root+'/bepeho.done')
    except:
        logging.exception('Got exception on main handler')
        os.rename(inFile, root+'/bepeho.error')
def main(root):
    for currentFolder in os.listdir(root):
        folderPath=root+'/'+currentFolder
        try:
            if not os.path.exists(folderPath+'/ready.to'):
                #logging.INFO("{} not ready to process".format(root))
                logging.info("not  ready")
                return 0
            processUploadToBepeho(folderPath)
            processUploadToYouTube(folderPath)
            os.rename(folderPath+'/ready.to', folderPath+'/ready.done')
            #videoIDs=upload(client, r+'videos/','video/mp4')
            
            return 0
        except:
            logging.exception('Got exception on main handler')
            os.rename(folderPath+'/ready.to', folderPath+'/ready.error')
            return 1


def get_parser():

    from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
    parser = ArgumentParser(description=__doc__,
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("-i", "--input",
                        dest="folderIn",
                        help="",
                        metavar="FILE",
                        required=True)
    parser.add_argument("-v", "--verbose",
                        dest="verbosity",
                        help="",
                        required=False)
    return parser


def _test():
    import doctest
    doctest.testmod()


if __name__ == "__main__":
    logger = logging.getLogger("bepeho")
    logger.setLevel(logging.INFO)
    #formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    #logger.setFormatter(formatter)
    try:
        args = get_parser().parse_args()

        if  args.verbosity is not None:
            # create a file handler
            handler = logging.FileHandler('bepeho.log')
            handler.setLevel(logging.INFO)
            
            # create a logging format
            formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
            handler.setFormatter(formatter)
            
            # add the handlers to the logger
            logger.addHandler(handler)
        
    except:
        logger.setLevel(logging.DEBUG)
        logger.addHandler(logging.StreamHandler(sys.stdout))
        _test()
    else:
        logging.info("starting")
        main(args.folderIn)
    exit (0)        